(in-package #:dht-view)

(defparameter *current-acceptor* nil)

(defun start-hunchentoot-server (port)
  (setf *current-acceptor* (make-instance 'hunchentoot:easy-acceptor :port port))
  (setf (hunchentoot:acceptor-document-root *current-acceptor*) ".")
  (hunchentoot:start *current-acceptor*))

(defmacro standard-page ((&key title) &body body)
  `(cl-who:with-html-output-to-string (*standard-output* nil :prologue t :indent t)
     (:html
      (:head
       (:meta :charset "utf-8")
       (:title ,title)
       (:link :type "text/css"
	      :rel "stylesheet"
	      :href "style/style.css"))
      (:body
       ,@body))))

(defun deploy-page ()
  (hunchentoot:define-easy-handler (dht-viewer :uri "/dht-viewer") ()
    (standard-page (:title "DHT Viewer")
      (:font :size "6" (cl-who:fmt "~a°C" (first (dht-read:get-current-reading))))
      (:font :size "5" (cl-who:fmt "~a%" (second (dht-read:get-current-reading))))
      (:font :size "4" (cl-who:fmt "~a" (third (dht-read:get-current-reading)))))))

(defun start-webserver (port)
  (start-hunchentoot-server port)
  (setf (cl-who:html-mode) :html5)
  (deploy-page))
