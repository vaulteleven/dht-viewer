(in-package #:dht-read)

;; these parameters may be system specific and represent the device
;; path of the dhtxx kernel driver module
(defparameter *sys-device-path* #P"/sys/bus/iio/devices/iio:device0/")
(defparameter *temperature-file* "in_temp_input")
(defparameter *humidity-file* "in_humidityrelative_input")

(defparameter *current-reading* nil)
(defparameter *readout-running-p* t)
(defparameter *readings-database* nil)

(defstruct dht-data
  (timestamp "" :type string)
  (human-timestamp "" :type string)
  (temperature 0.0 :type float)
  (humidity 0.0 :type float))

(defun print-dht-data (dht-data)
  (format nil "At ~a the temperature was ~a°C at a relative humidity of ~a%"
	  (dht-data-human-timestamp dht-data)
	  (dht-data-temperature dht-data)
          (dht-data-humidity dht-data)))

(defun get-current-timestamp ()
  (multiple-value-bind (second minute hour day month year)
    (get-decoded-time)
    (format nil "~2,'0d:~2,'0d:~2,'0d ~2,'0d.~2,'0d.~d" hour minute second day month year)))

(defun create-dht-data (temperature humidity)
  (make-dht-data :timestamp (format nil "~a" (get-universal-time))
		 :human-timestamp (get-current-timestamp)
		 :temperature (/ (read-from-string temperature) 1000.0)
		 :humidity (/ (read-from-string humidity) 1000.0)))

(defun get-sys-filesystem-path (filename)
  (format nil "~a~a" *sys-device-path* filename))

(defun read-sys-device-file (filepath)
  (let ((device-filepath (get-sys-filesystem-path filepath)))
    (if (probe-file device-filepath)
	(uiop:read-file-string device-filepath)
	"")))

(defun read-current-sensor-values ()
  (let ((temperature (read-sys-device-file *temperature-file*))
	(humidity (read-sys-device-file *humidity-file*)))
    (create-dht-data temperature humidity)))

(defun readout-loop (readout-interval-in-s)
  (loop while *readout-running-p* do
       (sleep readout-interval-in-s)
       (setf *current-reading* (read-current-sensor-values))
       (insert-reading-to-db *current-reading*)))

(defun start-readout (readout-interval-in-seconds)
  (connect-to-database "dhtxx-data")
  (bt:make-thread
   (lambda ()
     (readout-loop readout-interval-in-seconds)
     :name "dhtxx-readout")))

(defun connect-to-database (database-name)
  (setf *readings-database* (sqlite:connect database-name)))

(defun get-readings-from-db ()
  (sqlite:execute-to-list *readings-database* "select * from sensor_readings"))

(defun insert-reading-to-db (reading)
  (sqlite:execute-non-query *readings-database*
			    (format nil "insert into sensor_readings (temperature, humidity) values (~a, ~a)"
				    (dht-data-temperature reading)
				    (dht-data-humidity reading))))

(defun get-current-reading ()
  (car (sqlite:execute-to-list *readings-database* "select * from sensor_readings order by timestamp desc limit 1")))
