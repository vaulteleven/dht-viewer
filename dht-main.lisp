(in-package #:dht-main)

(defparameter *webserver-port* 10337)
(defparameter *readout-interval-in-seconds* 60)

(opts:define-opts
  (:name :help
	 :description "Print this help text."
	 :short #\h
	 :long "help")
  (:name :port
	 :description "The port on which the webserver will be running."
	 :short #\p
	 :long "port"
         :arg-parser #'parse-integer)
  (:name :interval
	 :description "The interval in seconds at which the sensor is read out. 
(values lower than 3 seconds are not possible because of hardware limitations of the sensor."
	 :short #\i
	 :long "interval"
         :arg-parser #'parse-integer))

(defun unknown-option (condition)
  (format t "~s option is unknown.~%" (opts:option condition))
  (opts:describe)
  (opts:exit))

(defun missing-arg (condition)
  (format t "Missing options: ~a needs an argument.~&" (opts:option condition))
  (opts:describe)
  (opts:exit))

(defun arg-parser-failed (condition)
  (format t "Error: could not parse ~a as an argument of ~a.~&"
	  (opts:raw-arg condition)
	  (opts:option condition))
  (opts:describe)
  (opts:exit))

(defun missing-required-option (condition)
  (format t "Error: missing options ~a are required ~a.~&"
	  (opts:missing-options condition)
	  (opts:option condition))
  (opts:describe)
  (opts:exit))

(defun handle-command-line-arguments ()
  (multiple-value-bind (options free-args)
      (handler-bind ((opts:missing-required-option #'missing-required-option)
		     (opts:unknown-option #'unknown-option)
		     (opts:missing-arg #'missing-arg)
		     (opts:arg-parser-failed #'arg-parser-failed))
	(opts:get-opts))
    (if (getf options :help)
	(progn
	  (opts:describe
	   :prefix "dhtxx. Usage:"
	   :args "[keywords]")
	  (opts:exit)))
    (if (getf options :port)
	(setf *webserver-port* (getf options :port)))
    (if (getf options :interval)
	(setf *readout-interval-in-seconds* (getf options :interval)))))

(defun init-webserver-and-readout ()
  (dht-read:start-readout *readout-interval-in-seconds*)
  (dht-view:start-webserver *webserver-port*))

(defun main ()
  (handle-command-line-arguments)
  (init-webserver-and-readout)
  (format t "Starting readout with interval ~a and server on port ~a~%" *readout-interval-in-seconds* *webserver-port*)
  (handler-case (bt:join-thread (find-if (lambda (th)
					   (search "hunchentoot" (bt:thread-name th)))
					 (bt:all-threads)))
    ;; Catch a user's C-c
    (#+sbcl sb-sys:interactive-interrupt
      #+ccl  ccl:interrupt-signal-condition
      #+clisp system::simple-interrupt-condition
      #+ecl ext:interactive-interrupt
      #+allegro excl:interrupt-signal
      () (progn
	   (format *error-output* "Aborting.~&")
	   (uiop:quit)))
    (error (c) (format t "Woops, an unknown error occured:~&~a~&" c))))
