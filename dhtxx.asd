;;;; dhtxx.asd

(asdf:defsystem #:dhtxx
  :description "Webserver that hosts DHT22 sensor data."
  :author "Hans Rosenkohl <hansrosenkohl@posteo.de>"
  :license  ""
  :version "0.0.1"
  :serial t
  :depends-on (#:hunchentoot 
               #:usocket 
               #:cl-json
               #:cl-who
	       #:parenscript
	       #:bt-semaphore
	       #:sqlite
	       #:unix-opts)
  :components ((:file "package")
               (:file "dht-view")
	       (:file "dht-main")
	       (:file "dht-read")))
